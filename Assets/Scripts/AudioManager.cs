using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase que gestiona los sonidos del juego
/// </summary>
public class AudioManager : MonoBehaviour
{
    [Header("Fuentes de Audio")]
    public AudioSource fx1;
    public AudioSource fx2;
    public AudioSource fx3;

    [Header("Clips de Audio")]
    public AudioClip musica;
    public AudioClip fxDisparo;
    public AudioClip fxExplosion;
    public AudioClip fxPropulsion;



    void ReproducirMusica() { }
    
    /// <summary>
    /// Reproduce el sonido de disparo
    /// </summary>
    public void Disparar() {
        fx1.clip = fxDisparo;
        fx1.Play();
    }

    /// <summary>
    /// Reproducir sonido de explosión
    /// </summary>
    public void Explosion() {
        fx2.clip = fxExplosion;
        fx2.Play();
    }

    /// <summary>
    /// Reproducir sonido de propulsión
    /// </summary>
    public void Propulsion() {
        
        if (!fx3.isPlaying)
        {
            fx3.clip = fxPropulsion;
            fx3.Play();
        }
        
    }

    /// <summary>
    /// Detener sonido de propulsión
    /// </summary>
    public void DetenerPropulsion()
    {
        
        if (fx3.isPlaying)
        {
            fx3.Stop();
        }
        
    }
    
}
