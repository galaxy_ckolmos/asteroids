using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase para los disparos de la nave
/// </summary>
public class Proyectil : MonoBehaviour
{
    /// <summary>
    /// Velocidad de desplazamiento
    /// </summary>
    public float velocidad = 10;

    /// <summary>
    /// Tiempo para destruirse
    /// </summary>
    public float tiempoDestruccion = 1;

    /// <summary>
    /// Rigidbody del proyectil
    /// </summary>
    Rigidbody rb;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        Propulsarse();

        Destroy(gameObject, tiempoDestruccion);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destruirse();
    }
    
    /// <summary>
    /// Se aplica una fuerza de propulsión
    /// </summary>
    void Propulsarse()
    {
        rb.AddRelativeForce(Vector3.forward*velocidad);
    }

    /// <summary>
    /// Destrucción del proyectil
    /// </summary>
    void Destruirse()
    {
        Destroy(gameObject);
    }


}
